<?php

namespace App\Listeners;

use App\Events\EmailConfirmation;
use App\Mailers\AppMailer;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendEmailConfirmationToUser implements ShouldQueue
{

    private $mailer;

    /**
     * Create the event listener.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer  = new AppMailer($mailer);
    }

    /**
     * Handle the event.
     * @param EmailConfirmation  $event
     * @return void
     */
    public function handle(EmailConfirmation $event)
    {
        $this->mailer->sendEmailConfirmationToUser($event->user);
    }
}
