<?php

namespace App\Mailers;

use App\User;
use Illuminate\Contracts\Mail\Mailer;

class AppMailer
{
    protected $from = 'admin@umedics.com.ng';

    protected $to;

    protected  $view;

    protected  $data = [];

    protected $mailer;


    public function  __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function sendEmailConfirmationToUser(User $user)
    {
        $this->to = $user->email;

        $this->view = 'emails.confirm';

        $this->data = compact('user');

        $this->deliver();
    }


    public function deliver()
    {
        $this->mailer->send($this->view,$this->data, function($message){
            $message->from($this->from, 'Faniyi Cornelius')
                    ->subject('Welcome to umedics')
                    ->to($this->to);
        });
    }
}