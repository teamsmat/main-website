<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReleasedProduct extends Model
{
    protected $table = 'releasedProducts';
}
