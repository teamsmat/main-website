<?php

namespace App;

use Auth;
use Carbon\carbon;
use Illuminate\Database\Eloquent\Model;

class UserMetaData extends Model
{
    protected $table = 'user_meta_data';

    public function saveData(Array $data)
    {
        foreach ($data as $key => $value) {
            $detail = $this->where('meta_data_title', $key)
                ->where('meta_data_value', '<>', null)
                ->where('user_id', Auth::user()->id)
                ->first();
            if (count($detail)) {
                continue;
            }
            $this->insert([
                'user_id' => Auth::user()->id,
                'meta_data_title' => $key,
                'meta_data_value' => $value,
                'meta_data_active' => true,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
