<?php

namespace App;

use Auth;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_name', 'therapeutic_class', 'qty','cost_price','selling_price','mfg_date','exp_date','batch_no','product_owner'
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function($product) {
            $product->product_owner = Auth::user()->id;
            $product->active = true;
        });
    }

    public function deleteProduct($data)
    {
        foreach($data as $id)
        {
            $product = $this->findOrFail($id);
            $product->deleted = true;
            $product->active = false;
            $product->save();
        }
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function release($data)
    {
        foreach($data as $id)
        {
            $product = ReleasedProduct::where('product_id',$id)->first();
            if($product) continue;
            (new ReleasedProduct())->insert(['product_id'=>$id]);
        }
    }
}
