<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{

	private $_instance,
			$_data;

	protected $table = 'emailSubscribers';

    protected $fillable = ['email','details'];

    public function subscribers()
    {
    	return $this->get();
    }


    public function subscriber($key)
    {
    	return $this->findOrFail($key)->first();
    }


    public function addNewSubscriber($data)
    {    	
    	if(!$this->create($data))
    	{
    		throw new Exception("An Error Occured", 1);
    	}    	
    }


    public function sendMail($message)
    {
    	/* Fire an Event and pass on the message */
    }


    public static function init()
    {
    	return new Newsletter();
    }


}
