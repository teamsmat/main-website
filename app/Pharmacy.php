<?php

namespace App;

use Auth;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
    protected $table = 'users';

    protected $hidden = [
        'password', 'remember_token','token',
    ];

    private $user;

    public function __construct()
    {
        $this->user = $this->where('group',2);
    }

    public function getAll()
    {
        return $this->user;
    }

    public function allConnections()
    {
        return $this->getAll()->where('id','<>',Auth::user()->id);
    }

    public function getLocation()
    {
        $client = new Client();
        $internetData = $client->get('http://ipinfo.io');
        return json_decode($internetData->getBody())->loc;
    }
}
