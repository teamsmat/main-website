<?php

namespace App\Http\Controllers;

use App\Pharmacy;
use Auth;
use App\UserMetaData;
use App\Http\Requests;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function getDashboard()
    {
        $title = 'Dashboard';
        $productsCount = $this->user->products->count();
        $expirationCount = $this->user->productsExpiring()->count();
        $finishingCount = $this->user->productsFinishing()->count();

        $issueCount = $expirationCount + $finishingCount;

        return view('admin.dashboard',compact('title','productsCount','issueCount'));
    }

    public function setup(Request $request)
    {
        $title = 'Setup your account';
        return view('admin.setup',compact('title'));
    }

    public function postSetup(Request $request, UserMetaData $userMetaData)
    {
        $meta_data = $request->except(['username']);

        if($request->has('username') && !is_null($request->input('username')))
        {
            $this->user->username = $request->get('username');
            $this->user->save();
        }

        $userMetaData->saveData($meta_data);

        if($request->has('location') && $request->input('location') == true)
        {
            $loc = (new Pharmacy())->getLocation();
            $meta_data = new UserMetaData();
            $meta_data->user_id = Auth::user()->id;
            $meta_data->meta_data_title = 'location';
            $meta_data->meta_data_value = $loc;

            $meta_data->save();
        }

        return 'OK';
    }

    public function settings()
    {
        $metaData = $this->user->metaData;
        $title = 'Settings...';
        return view('admin.settings',compact('title','metaData'));
    }

}
