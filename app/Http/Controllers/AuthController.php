<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\UserType;
use App\Http\Requests;
use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use App\Http\Requests\RegistrationRequest;


class AuthController extends Controller
{
    public function confirmEmail($token)
    {
        User::whereToken($token)->firstOrFail()->confirmEmail();

        return redirect('login');
    }

    public function getLogin()
    {
    	return view('auth.login');
    }

    public function getRegister()
    {
        $userType = UserType::all();
        return view('auth.register', compact('userType'));
    }

    public function postLogin(Request $request)
    {
        $this->validate($request,[
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password,'verified'=>1])) {
            return $this->redirectBackWithError('username and password combination incorrect');
        }

        $user = Auth::user();

        if(is_null($user->username))
        {
            return redirect('/'.Auth::user()->id.'/dashboard');
        }

        return redirect('/'.Auth::user()->username.'/dashboard');
    }

    public function postRegister(RegistrationRequest $request, AppMailer $mailer)
    {
    	try
    	{
    		$user = User::create($request->all());

            $mailer->sendEmailConfirmationToUser($user);

    		return redirect()->back()->with('success','An Email confirmation link has been sent to '.$user->email.'. Click the link to activate your account');
    	}catch(Exception $e){
            dd($e->getMessage());
            /*Fire an event to the admin that an action has just failed*/
            $this->redirectBackWithError($e);
    	}
    }

    public function redirectBackWithError($e)
    {
        if(is_object($e))
        {
            return redirect()->back()->with('error',$e->getMessage());
        }
        return redirect()->back()->with('error',$e);
    }
}
