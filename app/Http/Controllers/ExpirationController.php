<?php

namespace App\Http\Controllers;

use App\ReleasedProduct;
use Auth;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;

class ExpirationController extends Controller
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(User $user)
    {
        if($this->request->ajax())
        {
            return $user->find(Auth::user()->id)->productsExpiring;
        }
        $title = 'All product expiring soon';
        return view('admin.allissues',compact('title'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function released(ReleasedProduct $releasedProduct)
    {
        if($this->request->ajax())
        {
            return $releasedProduct->lists('product_id');
        }
    }
}
