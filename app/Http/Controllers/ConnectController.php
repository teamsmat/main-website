<?php

namespace App\Http\Controllers;

use Auth;
use App\Pharmacy;
use App\Http\Requests;
use Illuminate\Http\Request;
class ConnectController extends Controller
{
    private $user,
            $request,
            $pharmacy;

    public function __construct(Request $request,Pharmacy $pharmacy)
    {
        $this->request = $request;
        $this->pharmacy = $pharmacy;
        $this->user = Auth::user();
    }

    public function index()
    {
        $connections = $this->pharmacy->allConnections()->paginate(15);
        $title = 'Connect with other pharmacies across Nigeria';
        return view('admin.connect',compact('title','connections'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
