<?php

namespace App\Http\Controllers;

use Auth;
use App\Newsletter;
use App\Http\Requests;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }


    public function postNewsletter(Request $request)
    {
        if($request->ajax())
        {
            return json_encode($request->all());
        }

        try
        {
            Newsletter::init()->addNewSubscriber($request->except('_token'));
            return redirect()->back()->withSuccess('Newsletter Inserted Successfully');
        }catch(Exception $e)
        {
            return redirect::back()->with('error',$e->getMessage);
        }
    }


    public function gettingStarted()
    {
        return view('pages.getting_started');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
