<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductsController extends Controller
{
    private $product,
            $request,
            $user;

    public function __construct(Request $request, Product $product)
    {
        $this->product = $product;
        $this->request = $request;
        $this->user = Auth::user();
    }

    public function index(User $user)
    {
        if($this->request->ajax())
        {
            return $this->user->products;
        }
        $title = 'All products';
        return view('admin.allproducts',compact('title'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id, $data)
    {
        $data = json_decode(html_entity_decode($data));
        $this->product->deleteProduct($data);
    }

    public function newProduct()
    {
        $data = $this->product->create($this->request->all());
        return $data;
    }

    public function releaseProduct($id)
    {
        return $this->product->release($this->request->all());
    }

    public function finishing()
    {
        if($this->request->ajax())
        {
            return $this->user->productsFinishing();
        }

        $title = 'Out of Stock';
        return view('admin.finishingProducts',compact('title'));

    }
}
