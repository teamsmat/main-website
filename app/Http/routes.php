<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
*/

# Register all get requests here.

Route::get('/', 'PagesController@index');
Route::get('login','AuthController@getLogin');
Route::get('logout','PagesController@logout');
Route::get('contact','PagesController@contact');
Route::get('register','AuthController@getRegister');
Route::get('get-started','PagesController@gettingStarted');
Route::get('register/confirm/{token}','AuthController@confirmEmail');


# Register all post requests here

Route::post('login','AuthController@postLogin');
Route::post('register','AuthController@postRegister');
Route::post('newsletter', 'PagesController@postNewsletter');


# Register all admin routes here
Route::group(['prefix'=> '{userId}','middleware' => ['auth','pharmacy']],function(){

    Route::get('setup','AdminController@setup');
    Route::post('setup','AdminController@postSetup');
    Route::group(['middleware'=>'setup'],function(){

        Route::get('dashboard','AdminController@getDashboard');

        Route::get('products/finishing','ProductsController@finishing');
        Route::post('products/newProduct','ProductsController@newProduct');
        Route::post('products/release','ProductsController@releaseProduct');
        Route::resource('products','ProductsController');

        Route::get('expiration/released','ExpirationController@released');
        Route::resource('expiration','ExpirationController');

        Route::resource('connect','ConnectController');

        Route::get('settings','AdminController@settings');

    });

});








