<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $fillable = [
        'name', 'email', 'password','group'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($user){
            $user->token = str_random(30);
        });
    }

    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;
        $this->save();
    }

    public function fetchAllProducts(Product $product)
    {
        return $this->products()
                    ->get();
    }

    public function productsFinishing()
    {
        return $this->products()
                    ->where('qty','<=',20)
                    ->get();
    }

    public function isPharmacy()
    {
        return $this->group == 2;
    }

    public function metaData() {
        return $this->hasMany('App\UserMetaData');
    }

    public function products()
    {
        return $this->hasMany('App\Product','product_owner')
                    ->where('active',1)
                    ->where('deleted',0);
    }

    public function productsExpiring()
    {
        $futureTime = Carbon::now()->addMonth(pmen());
        return $this->products()
                    ->whereBetween('exp_date',[Carbon::now(), $futureTime]);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
}
