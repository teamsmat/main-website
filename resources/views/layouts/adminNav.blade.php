<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li class="nav-active">
                        <a href="/{{ Auth::user()->username }}/dashboard">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <hr />

                    <li>
                        <a href="/{{ Auth::user()->username }}/connect">
                            <i class="fa fa-plug" aria-hidden="true"></i>
                            <span>Connect</span>
                        </a>
                    </li>

                    <li>
                        <a href="/{{ Auth::user()->username }}/expiration">
                            <span class="pull-right label label-danger">@{{ expiration.length }}</span>
                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                            <span>Expirations</span>
                        </a>
                    </li>

                    <li>
                        <a href="/{{ Auth::user()->username }}/products/finishing">
                            <span class="pull-right label label-warning">@{{ outOfStock.length }}</span>
                            <i class="fa fa-exclamation" aria-hidden="true"></i>
                            <span>Out of Stock</span>
                        </a>

                    </li>

                    <li>
                        <a href="/{{ Auth::user()->username }}/products">
                            <span class="pull-right label label-primary">@{{ products.length }}</span>
                            <i class="fa fa-gift" aria-hidden="true"></i>
                            <span>Products</span>
                        </a>
                    </li>

                    <li>
                        <a href="/{{ Auth::user()->username }}/settings">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            <span>Settings</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a href="#">
                                    Profile
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Users
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    General Settings
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#!">
                            <i class="fa fa-credit-card" aria-hidden="true"></i>
                            <span>Subscription</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a href="#">
                                    Subscription Plan
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Payments
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Cards
                                </a>
                            </li>
                        </ul>
                    </li>

                    {{--<li>
                        <a href="/s/{{ Auth::user()->username }}" target="_blank">
                            <i class="fa fa-external-link" aria-hidden="true"></i>
                            <span>Your public profile</span>
                        </a>
                    </li>--}}

                </ul>
            </nav>

            {{--<hr class="separator" />

            <div class="sidebar-widget widget-tasks">
                <div class="widget-header">
                    <h6> Top Priorities</h6>
                    <div class="widget-toggle">+</div>
                </div>
                <div class="widget-content">
                    <ul class="list-unstyled m-none">
                        <li><a href="#">2 Products Expiring Soon</a></li>
                        <li><a href="#">1 person followed you</a></li>
                    </ul>
                    <p>None at the Moment</p>
                </div>
            </div>

            <hr class="separator" />

            <div class="sidebar-widget widget-stats">
                <div class="widget-header">
                    <h6>Pharmacy Stats</h6>
                    <div class="widget-toggle">+</div>
                </div>
                <div class="widget-content">
                    <ul>
                        <li>
                            <span class="stats-title"> Customer Engagement </span>
                            <span class="stats-complete">85%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
                                    <span class="sr-only">85% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span class="stats-title">Profile Views</span>
                            <span class="stats-complete">70%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                    <span class="sr-only">70% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span class="stats-title">Drugs Expiration Disposal</span>
                            <span class="stats-complete">2%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
                                    <span class="sr-only">2% Complete</span>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <p>None at the moment</p>
                </div>
            </div>--}}
        </div>

    </div>

</aside>
<!-- end: sidebar -->