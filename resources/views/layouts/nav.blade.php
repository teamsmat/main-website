<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navigation">
                <span class="icon-navicon"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/assets/img/umedics-main.png" data-dark-src="/assets/img/umedics.png" alt="Umedics Logo" class="logo">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-navigation">
            <ul class="nav navbar-nav navbar-right">

                <li><a href="/" class="{{ Request::path() == '/' ? 'active' : '' }}">HOME</a></li>
                <li><a href="/get-started" class="{{ Request::path() == 'get-started' ? 'active' : '' }}">GETTING STARTED</a></li>
                <li><a href="/get-started#packages" class="{{ Request::path() == 'get-started#packages' ? 'active' : '' }}">PRICING</a></li>
                <li><a href="/contact" class="{{ Request::path() == 'contact' ? 'active' : '' }}">CONTACT</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-->
</nav>