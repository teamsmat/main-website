<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Umedics - Home</title>  
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Free drug searching service, Nigeria">
        <meta name="keywords" content="Umedics, Find drugs around you, Manage your health">
        <meta name="author" content="Umedics by Quick Age Concepts">

        <link rel="stylesheet" type="text/css" href="/css/all.css">
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="assets/img/favicon.ico">
        <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-touch-icon-152x152.png" />    
    </head>

    <body class="">

        <div id="wrapper">    

            <header>
                <div id="topbar">
                	<div class="container">
                		<div class="row">
                			<div class="col-sm-6 col-xs-6">
                			<span class="hidden-sm hidden-xs"><i class="icon-location4"></i>14, Lalubu Street, Ogun State Nigeria</span><span class="vertical-space"></span> <i class="icon-phone4"></i>07058286311
                			</div>
                			<div class="col-sm-6 col-xs-6 text-right">
                				<div class="btn-group social-links hidden-sm hidden-xs">	
                					<a href="javascript:;" class="btn btn-link"><i class="icon-facebook4"></i></a>
                					<a href="javascript:;" class="btn btn-link"><i class="icon-twitter4"></i></a>
                				</div>
                				<a href="/login" class="login-button {{ Request::path() == 'login' ? 'active' : '' }}">LOGIN</a><a href="/register" class="signup-button">SIGN UP</a>
                			</div>
                		</div>
                		<div class="top-divider"></div>
                	</div>
                </div>            

                @include('layouts.nav')

            </header>

            @yield('content')

            <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <a href="/">
                                    <img src="/assets/img/umedics.png" alt="Umedics Logo" class="footer-logo">
                                </a>
                                <h5>you are healthier</h5>
                                <br>

                                
                            </div>


                            <div class="col-sm-4">
                                <h4>CONTACT US</h4>
                                <ul class="list-unstyled company-info">
                                    <li><i class="icon-map-marker"></i>14, Lalubu Street, Oke-Ilewo Abeokuta <br>Ogun State Nigeria</li>
                                    <li><i class="icon-phone3"></i> 07058286311</li>
                                    <li><i class="icon-envelope"></i> <a href="mailto:info@umedics.com.ng">info@umedics.com.ng</a></li>
                                    <li><i class="icon-alarm2"></i> Monday - Friday: <strong>8:00 am - 7:00 pm</strong><br>Saturday - Sunday: <strong>Closed</strong></li>
                                </ul>
                            </div>
                            
                            <div class="col-sm-4 hidden-xs">
                                <h4>LINKS</h4>
                                <!-- <ul class="list-unstyled social-stream">
                                    <li><i class="icon-twitter4"></i> We just released an awesome frontend template for our coco template. Come on and check it out! - <a href="http://goo.gl/ylVWzR">http://goo.gl/ylVWzR</a><br><span class="text-default text-sm">Oct 20</span></li>
                                    <li><i class="icon-twitter4"></i> Our template is going popular. Don't miss it!<br><span class="text-default text-sm">Oct 19</span></li>
                                    <li><i class="icon-facebook4"></i> World is becoming a crazy place. Try to avoid toxic materials.<br><span class="text-default text-sm">Oct 19</span></li>
                                </ul> -->

                                <ul class="list-unstyled social-stream">
                                    <li><a href="/frequently-asked-questions" title="Frequently Asked Questions">FAQ's</a></li>
                                    <li><a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
                                    <li><a href="/blog" title="Blog">Blog</a></li>
                                    <li><a href="/offline-access" title="Offline Access">Offline Access</a></li>
                                    
                                </ul>

                            </div>
                        </div>
                        <hr>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <p>Copyright &copy; {{ date('Y') }} by <a href="http://www.umedics.com.ng" target="_blank">The UMEDICS TEAM</a></p> 
                            </div>
                            <div class="col-sm-6 text-right">
                                <div class="social-links">
                                    <a href="http://twitter.com/umedics"target="blank" >
                                        <i class="icon-twitter4"></i>
                                    </a>
                                    {{--<a href="javascript:;">
                                        <i class="icon-facebook4"></i>
                                    </a>
                                    <a href="javascript:;">
                                        <i class="icon-googleplus6"></i>
                                    </a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
            </footer>
            <a class="btn btn-info btn-circle btn-fixed" title="Search Umedics">
                <i class="icon-search"></i>&nbsp;&nbsp;&nbsp;&nbsp; <i class="icon-angle-right"></i>
            </a>
            <a class="tothetop" href="javascript:;"><i class="icon-angle-up"></i></a>
        </div>

        <script>
            var resizefunc = [];
        </script>
        <script src="/js/all.js"></script>

        @yield('scripts')
    </body>
</html>