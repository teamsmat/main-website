<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <meta name="token" id="token" value="{{ csrf_token() }}">

    <title>{{ ucfirst(Auth::user()->username) }} - {{ $title }} </title>

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/admin-assets/vendor/bootstrap/css/bootstrap.css" />

    <link rel="stylesheet" href="/admin-assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="/admin-assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="/admin-assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="/admin-assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
    <link rel="stylesheet" href="/admin-assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
    <link rel="stylesheet" href="/admin-assets/vendor/morris/morris.css" />
    <link rel="stylesheet" href="/css/main.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="/admin-assets/stylesheets/theme.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="/admin-assets/vendor/pnotify/pnotify.custom.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="/admin-assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="/admin-assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="/admin-assets/vendor/modernizr/modernizr.js"></script>
</head>
<body>
<section class="body">

    <!-- start: header -->
    <header class="header">
        <div class="logo-container">
            <a href="../" class="logo">
                <img src="/assets/img/umedics-main.png" height="35" alt="Umedics Logo" />
            </a>
            <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="header-left" style="float:left;width:200px;">
            <h4>@{{ userDetails.provider_name }}</h4>
        </div>
        <!-- start: search & user box -->
        <div class="header-right">
            <form action="/search" class="search nav-form">
                <div class="input-group input-search">
                    <input type="text" class="form-control" name="q" id="q" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
							</span>
                </div>
            </form>

            <span class="separator"></span>

            <ul class="notifications">
                {{--<li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="fa fa-tasks"></i>
                    </a>

                    <div class="dropdown-menu notification-menu large">
                        <div class="notification-title">
                            Tasks
                        </div>

                        <div class="content">

                            <p>No tasks yet...</p>

                        </div>
                    </div>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="fa fa-envelope"></i>
                    </a>

                    <div class="dropdown-menu notification-menu">
                        <div class="notification-title">
                            Messages
                        </div>

                        <div class="content">

                            <p>No Messages yet...</p>

                        </div>
                    </div>
                </li>--}}
                <li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="fa fa-bell"></i>
                    </a>

                    <div class="dropdown-menu notification-menu">
                        <div class="notification-title">
                            Notifications
                        </div>

                        <div class="content">

                            <p>No Notifications yet...</p>

                        </div>
                    </div>
                </li>
            </ul>

            <span class="separator"></span>

            <div id="userbox" class="userbox">
                <a href="#" data-toggle="dropdown">
                    <figure class="profile-picture">
                        <img src="/admin-assets/images/!logged-user.jpg" alt="{{ Auth::user()->name  }}" class="img-circle" data-lock-picture="/admin-assets/images/!logged-user.jpg" />
                    </figure>
                    <div class="profile-info" data-lock-name="{{ Auth::user()->name }}" data-lock-email="{{ Auth::user()->email }}">
                        <span class="name">{{ Auth::user()->name  }}</span>
                        <span class="role">pharmacy</span>
                    </div>

                    <i class="fa custom-caret"></i>
                </a>

                <div class="dropdown-menu">
                    <ul class="list-unstyled">
                        <li class="divider"></li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Application</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="/logout"><i class="fa fa-power-off"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end: search & user box -->
    </header>
    <!-- end: header -->

    <div class="inner-wrapper">

        {{-- THe Navigation Bar --}}
        @include('layouts.adminNav')

        @yield('content')

    </div>

</section>

<!-- Vendor -->
<script src="/js/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.min.js"></script>
<script src="/admin-assets/vendor/jquery/jquery.js"></script>
<script src="/admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="/admin-assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="/admin-assets/vendor/nanoscroller/nanoscroller.js"></script>
<script src="/admin-assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/admin-assets/vendor/magnific-popup/magnific-popup.js"></script>
<script src="/admin-assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="/admin-assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
<script src="/admin-assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
<script src="/admin-assets/vendor/jquery-appear/jquery.appear.js"></script>
<script src="/admin-assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="/admin-assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
<script src="/admin-assets/vendor/flot/jquery.flot.js"></script>
<script src="/admin-assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
<script src="/admin-assets/vendor/flot/jquery.flot.pie.js"></script>
<script src="/admin-assets/vendor/flot/jquery.flot.categories.js"></script>
<script src="/admin-assets/vendor/flot/jquery.flot.resize.js"></script>
<script src="/admin-assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
<script src="/admin-assets/vendor/raphael/raphael.js"></script>
<script src="/admin-assets/vendor/morris/morris.js"></script>
<script src="/admin-assets/vendor/gauge/gauge.js"></script>
<script src="/admin-assets/vendor/snap-svg/snap.svg.js"></script>
<script src="/admin-assets/vendor/liquid-meter/liquid.meter.js"></script>
<script src="/admin-assets/vendor/jqvmap/jquery.vmap.js"></script>
<script src="/admin-assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
<script src="/admin-assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/admin-assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="/admin-assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/admin-assets/javascripts/theme.init.js"></script>

<!-- Specific Page Vendor -->
<script src="/admin-assets/vendor/pnotify/pnotify.custom.js"></script>

<!-- Examples -->
<script src="/admin-assets/javascripts/dashboard/examples.dashboard.js"></script>
<script src="/js/ajax.js"></script>

@yield('page_scripts')
</body>
</html>