@extends('layouts.master')


@section('content')

	<section class="main-slider fullsize" data-stellar-background-ratio="0.5" style="background-image: url(/images/headers/signup-login.jpg)">
		<div class="slider-caption">
			
		    <div class="container">
		        <div class="row">
		            <div class="col-sm-4 col-sm-offset-4">
		                <form class="form-signin" role="form" action="/login" method="POST">

                            {{ csrf_field() }}

							<h2 class="form-signin-heading">UMEDICS LOGIN</h2>

                            @include('layouts.errors')

			                <div class="form-group">
			                    <input type="email" class="form-control input-lg" name="email" placeholder="E-mail address" required autofocus>
			                </div>
			                <div class="form-group">
			                    <input type="password" class="form-control input-lg" name="password" placeholder="Password" required>
			                </div>
			                <button class="btn btn-lg btn-primary btn-block" type="submit">LOGIN</button><br>
			                Need an account? <a href="/register">Sign up</a> now!
			            </form>
		            </div>
		        </div>
		    </div>	
		</div>
	</section>

@stop