@extends('layouts.master')


@section('content')

	<section class="main-slider fullsize" data-stellar-background-ratio="0.5" style="background-image: url(images/headers/signup-login.jpg)">
		<div class="slider-caption">
			
		    <div class="container">
		        <div class="row">
		            <div class="col-sm-4 col-sm-offset-4">
		                <form class="form-signin" role="form" action="/register" method="POST" novalidate>
		                	{{  csrf_field()}}
			                <h2 class="form-signin-heading">SIGN UP</h2>

			                @include('layouts.errors')

			                <div class="form-group">

			                    <input type="email"
                                       class="form-control input-lg"
                                       placeholder="E-mail address"
                                       name="email"
                                       required
                                       autofocus
                                       value="{{ old('email') }}">
			                </div>

			                <div class="form-group">

			                    <input type="text"
                                       class="form-control input-lg"
                                       placeholder="Name"
                                       name="name"
                                       required
                                       value="{{ old('name') }}">
			                </div>

							<div class="form-group">

                                <select name="group"
                                        id="group"
                                        class="form-control">

                                        <option value=""><pre><em>I am a?</em></pre></option>

										@foreach($userType as $type)

											<option value="{{ $type->id }}">{{ ucfirst($type->type_name) }}</option>

										@endforeach

                                </select>
							</div>

			                <div class="form-group">

			                    <input type="password"
                                       class="form-control input-lg"
                                       placeholder="Desired password"
                                       name="password"
                                       required>
			                </div>

			                <div class="form-group">
			                    <input type="password"
                                       class="form-control input-lg"
                                       placeholder="Confirm password"
                                       name="confirm_password"
                                       required>
			                </div>

			                <button class="btn btn-lg btn-primary btn-block" type="submit">REGISTER</button>

                            <br>

			                Already have an account? <a href="/login">Login</a> now!

			            </form>
		            </div>
		        </div>
		    </div>	
		</div>
	</section>

@stop