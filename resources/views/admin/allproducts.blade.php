@extends('layouts.admin')

@section('content')
    <section role="main" class="content-body">
        <div class="row">
            <div class="col-sm-8">
                <a class="btn btn-default mb-xs mt-xs mr-xs" v-show="!selectedExists()" v-on:click="refresh"><i class="fa fa-refresh"></i></a>
                <a class="btn btn-primary mb-xs mt-xs mr-xs" data-toggle="modal" data-target="#modalBootstrap">Add New <i class="fa fa-plus"></i></a>
                <a class="btn btn-success modal-with-move-anim mb-xs mt-xs mr-xs" href="#newProductsForm">Upload Products <i class="fa fa-cloud-upload"></i></a>
                <a class="btn btn-default mb-xs mt-xs mr-xs" v-show="selectedExists()">Export <i class="fa fa-cloud-download"></i></a>
                <a class="btn btn-danger mb-xs mt-xs mr-xs" v-show="selectedExists()" @click="deleteSelected">Delete <i class="fa fa-trash"></i></a>
            </div>

            <div id="newProductsForm" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide ">
                @include('admin.uploadProducts')
            </div>

            <div class="modal fade" id="modalBootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                @include('admin.newProduct')
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    </div>

                    <h2 class="panel-title">All Products</h2>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table mb-none" v-if="productExists()">
                            <thead>
                            <tr>
                                <th><input type="checkbox" v-model="selectAll" @click="checkAll"></th>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Therapeutic Class</th>
                                <th>Expiry Date</th>
                                <th>Qty Available</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(product,index) in products" v-bind:class="{selected:isSelected(product.id)}"  class="products">
                                    <td><input type="checkbox" v-bind:value="product.id" v-bind:checked="selectAll" v-model="selected"></td>
                                    <td>@{{ index+1 }}</td>
                                    <td>@{{ product.product_name }}</td>
                                    <td>@{{ product.therapeutic_class }}</td>
                                    <td></td>
                                    <td>@{{ product.qty }}</td>
                                    <td class="actions-hover actions-fade">
                                        <a href=""><i class="fa fa-pencil"></i></a>
                                        <a href="#!" class="delete-row" @click="deleteSingle(product.id)"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p v-if="!productExists()">You do not have any products yet...</p>
                    </div>
                </div>
            </section>
            </div>
        </div>
    </section>
@stop

@section('page_scripts')
    <script src="/admin-assets/javascripts/ui-elements/examples.modals.js"></script>
    <script src="/admin-assets/javascripts/forms/examples.advanced.form.js"></script>
@stop