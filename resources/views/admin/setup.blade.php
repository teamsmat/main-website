@extends('layouts.admin')

@section('content')

    <section role="main" class="content-body">

        <div class="row">
        <div class="col-xs-12">
            <section class="panel form-wizard" id="w4">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                    </div>

                    <h2 class="panel-title">Account Information</h2>
                </header>
                <div class="panel-body">
                    <div class="wizard-progress wizard-progress-lg">
                        <div class="steps-progress">
                            <div class="progress-indicator"></div>
                        </div>
                        <ul class="wizard-steps">
                            <li class="active">
                                <a href="#w4-profile" data-toggle="tab"><span>1</span>Company Info</a>
                            </li>
                            <li>
                                <a href="#w4-billing" data-toggle="tab"><span>2</span>Billing Info</a>
                            </li>
                            <li>
                                <a href="#w4-confirm" data-toggle="tab"><span>3</span>Confirmation</a>
                            </li>
                        </ul>
                    </div>

                    <form class="form-horizontal" novalidate="novalidate">
                        <div class="tab-content">

                            <div id="w4-profile" class="tab-pane active">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w4-first-name">PCN No:</label>
                                    <div class="col-sm-4">
                                        <input v-model="userDetails.pcn_no" type="text" class="form-control" name="w4-first-name" id="w4-first-name" title="We only need it to verify you with pcn. We promise" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w4-pharmacy_name">Pharmacy Name:</label>
                                    <div class="col-sm-4">
                                        <input v-model="userDetails.provider_name" type="text" class="form-control" name="w4-pharmacy_name" id="w4-pharmacy_name" title="Enter your pharmacy name as registered with the PCN" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w4-username">Username:</label>
                                    <div class="col-sm-4">
                                        <input v-model="userDetails.username" type="text" class="form-control" name="username" id="w4-username" placeholder="e.g umedics_stores" title="This will be used to identify by many users" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w4-size">Company Size:</label>
                                    <div class="col-sm-4">
                                        <select v-model="userDetails.size" class="form-control" name="size" id="w4-size" required>
                                            <option>Hospital Pharmacy</option>
                                            <option>Roadside Pharmacy</option>
                                        </select>
                                        {{--<input type="text" class="form-control" name="size" id="w4-size" required>--}}
                                    </div>
                                </div>
                            </div>
                            <div id="w4-billing" class="tab-pane">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w4-cc">Card Number</label>
                                    <div class="col-sm-6">
                                        <input v-model="userDetails.card_no" type="text" class="form-control" name="cc-number" id="w4-cc" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputSuccess">Expiration</label>
                                    <div class="col-sm-3">
                                        <select v-model="userDetails.card_exp_month" class="form-control" name="exp-month" required>
                                            <option>January</option>
                                            <option>February</option>
                                            <option>March</option>
                                            <option>April</option>
                                            <option>May</option>
                                            <option>June</option>
                                            <option>July</option>
                                            <option>August</option>
                                            <option>September</option>
                                            <option>October</option>
                                            <option>November</option>
                                            <option>December</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select v-model="userDetails.card_exp_year" class="form-control" name="exp-year" required>
                                            <option>2014</option>
                                            <option>2015</option>
                                            <option>2016</option>
                                            <option>2017</option>
                                            <option>2018</option>
                                            <option>2019</option>
                                            <option>2020</option>
                                            <option>2021</option>
                                            <option>2022</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cvv">CVV Number</label>
                                    <div class="col-sm-6">
                                        <input type="text" v-model="userDetails.cvv_no" class="form-control" name="cvv" id="w4-cvv" required>
                                    </div>
                                </div>
                            </div>
                            <div id="w4-confirm" class="tab-pane">
                                {{--<div class="form-group">
                                    <label class="col-sm-3 control-label" for="w4-email">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="email" id="w4-email" required>
                                    </div>
                                </div>--}}
                                <div class="form-group">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9">
                                        <div class="checkbox-custom">
                                            <input type="checkbox" name="terms" id="w4-terms" v-model="termsOfService" required>
                                            <label for="w4-terms">I agree to the terms of service</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <ul class="pager">
                        <li class="previous disabled">
                            <a><i class="fa fa-angle-left"></i> Previous</a>
                        </li>
                        <li class="finish hidden pull-right">
                            <a v-on:click="finishSetup">Finish</a>
                        </li>
                        <li class="next">
                            <a>Next <i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>

    </section>

@stop

@section('page_scripts')
    <script src="/admin-assets/vendor/jquery-validation/jquery.validate.js"></script>
    <script src="/admin-assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
    <script src="/admin-assets/vendor/pnotify/pnotify.custom.js"></script>
    <script src="/admin-assets/javascripts/forms/examples.wizard.js"></script>
@stop