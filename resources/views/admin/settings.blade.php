@extends('layouts.admin')

@section('content')

    <section role="main" class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#general" data-toggle="tab"><i class="fa fa-cog"></i> General Settings</a>
                        </li>
                        <li>
                            <a href="#profile" data-toggle="tab"><i class="fa fa-profile"></i> Company Profile</a>
                        </li>
                        <li>
                            <a href="#users" data-toggle="tab"><i class="fa fa-users"></i> Application Users</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active">
                            @include('admin.settings.general')
                        </div>
                        <div id="profile" class="tab-pane">
                            @include('admin.settings.profile')
                        </div>
                        <div id="users" class="tab-pane">
                            <p>Add a new user or update your authenticated users</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop