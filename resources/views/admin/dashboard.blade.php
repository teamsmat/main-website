@extends('layouts.admin')

@section('content')



    <section role="main" class="content-body">

        <!-- start: page -->
        <div class="row">

            <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="row">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-primary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-primary">
                                            <i class="fa fa-gift"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Total Products</h4>
                                            <div class="info">
                                                <strong class="amount">{{ $productsCount }}</strong>
                                                <span class="text-primary">(14 searches)</span>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(view all)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-tertiary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-tertiary">
                                            <i class="fa fa-usd"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">N/A</h4>
                                            <div class="info">
                                                <strong class="amount">&#8358; {{ number_format(200050) }}</strong>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(withdraw)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-secondary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-secondary">
                                            <i class="fa fa-exclamation"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Products with Issues</h4>
                                            <div class="info">
                                                <strong class="amount">{{ $issueCount }}</strong>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(view all)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-quartenary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-quartenary">
                                            <i class="fa fa-inbox"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Today's Subscribers</h4>
                                            <div class="info">
                                                <strong class="amount">4</strong>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(report)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xl-6 col-lg-12">
                <section class="panel">
                    <header class="panel-heading panel-heading-transparent">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>

                        <h2 class="panel-title">Company Activity</h2>
                    </header>
                    <div class="panel-body">
                        <div class="timeline timeline-simple mt-xlg mb-md">
                            <div class="tm-body">
                                <div class="tm-title">
                                    <h3 class="h5 text-uppercase">Since you last logged out</h3>
                                </div>
                                <ol class="tm-items">
                                    {{--<li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-none">7 months ago.</p>
                                            <p>
                                                It's awesome when we find a good solution for our projects, Porto Admin is <span class="text-primary">#awesome</span>
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-none">7 months ago.</p>
                                            <p>
                                                Checkout! How cool is that!
                                            </p>
                                            <div class="thumbnail-gallery">
                                                <a class="img-thumbnail lightbox" href="/admin-assets/images/projects/project-4.jpg" data-plugin-options='{ "type":"image" }'>
                                                    <img class="img-responsive" width="215" src="/admin-assets/images/projects/project-4.jpg">
																<span class="zoom">
																	<i class="fa fa-search"></i>
																</span>
                                                </a>
                                            </div>
                                        </div>
                                    </li>--}}
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

    </section>




@stop