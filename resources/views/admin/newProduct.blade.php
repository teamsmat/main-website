<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Add a New Product</h4>
        </div>
        <div class="modal-body">

            <form class="form-horizontal mb-lg">
                <div class="form-group mt-lg">
                    <label class="col-sm-3 control-label">Product Name</label>
                    <div class="col-sm-8">
                        <input v-model="newSingleProduct.product_name" type="text" name="p_name" class="form-control" placeholder="Enter product name" required/>
                    </div>
                </div>
                <div class="form-group mt-lg">
                    <label class="col-sm-3 control-label">Therapeutic Class</label>
                    <div class="col-sm-8">
                        <input v-model="newSingleProduct.therapeutic_class" type="text" name="p_class" class="form-control" placeholder="Enter the class the product belongs to" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Mfg Date</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input v-model="newSingleProduct.mfg_date" id="date" placeholder="YYYY-MM-DD" class="form-control" data-plugin-datepicker >
                        </div>
                    </div>

                    <label class="col-md-2 control-label">Exp Date</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input v-model="newSingleProduct.exp_date" id="date" placeholder="YYYY-MM-DD" class="form-control" data-plugin-datepicker>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Qty</label>
                    <div class="col-md-3">
                            <div class="input-group">
                                <input v-model="newSingleProduct.qty" type="number" class="form-control" max="100">
                            </div>
                    </div>

                    <label class="col-md-2 control-label">Batch No.</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <input v-model="newSingleProduct.batch_no" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Cost Price</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa">&#8358;</i>
                            </span>
                            <input v-model="newSingleProduct.cost_price" id="text"  placeholder="" class="form-control">
                        </div>
                    </div>

                    <label class="col-md-2 control-label">Selling Price</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa">&#8358;</i>
                            </span>
                            <input v-model="newSingleProduct.selling_price" id="text"  placeholder="" class="form-control">
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" @click="saveNewProduct" id="btnSaveProduct">Save Product</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>