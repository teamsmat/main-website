@extends('layouts.admin')

@section('content')
    <section role="main" class="content-body">
        <!-- start: page -->
        <div class="search-content">
            <div class="search-control-wrapper">
                <form action="pages-search-results.html">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search any pharmacy here">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="button">Search</button>
                                                </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="search-toolbar">
                <ul class="list-unstyled nav nav-pills">
                    <li class="active">
                        <a href="#everything" data-toggle="tab">Everything</a>
                    </li>
                    <li>
                        <a href="#medias" data-toggle="tab">Medias</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div id="everything" class="tab-pane active">
                    <p class="total-results text-muted">Showing 1 to 10 of 47 results</p>

                    <ul class="list-unstyled search-results-list">
                        @foreach($connections as $connect)

                            <li>
                                <p class="result-type">
                                    <span class="label label-primary">pharmacy</span>
                                </p>
                                <a href="/{{ Auth::user()->username }}/connect/{{ $connect->username }}" class="has-thumb">
                                    <div class="result-thumb">
                                        <img src="/admin-assets/images/!logged-user.jpg" alt="John Doe" />
                                    </div>
                                    <div class="result-data">
                                        <p class="h3 title text-primary">{{ $connect->name }}</p>
                                        <p class="description">Details of the pharmacy goes here</p>
                                    </div>
                                </a>
                            </li>

                        @endforeach

                    </ul>

                    <hr class="solid mb-none" />

                    {{ $connections->links() }}

                </div>
                <div id="medias" class="tab-pane">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="#">
                                        <img src="assets/images/projects/project-2.jpg" class="img-responsive" alt="Project">
                                    </a>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Blog<small>.png</small></h5>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="#">
                                        <img src="assets/images/projects/project-5.jpg" class="img-responsive" alt="Project">
                                    </a>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Friends<small>.png</small></h5>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="#">
                                        <img src="assets/images/projects/project-4.jpg" class="img-responsive" alt="Project">
                                    </a>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Life<small>.png</small></h5>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="#">
                                        <img src="assets/images/projects/project-5.jpg" class="img-responsive" alt="Project">
                                    </a>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Poetry<small>.png</small></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="emails" class="tab-pane">
                    <p>Recent</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                </div>
            </div>
        </div>
        <!-- end: page -->
    </section>
@stop
