<form class="form-horizontal form-bordered" method="POST">
    <div class="form-group">
        <label class="col-md-3 control-label">Months of Expiration Notice</label>
        <div class="col-md-4">
            <select class="form-control" v-model="generalInfo.pmen">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            {{--<input type="text" class="form-control" placeholder="Firm Name" v-model="userDetails.provider_name" required>--}}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Out of stock products limit</label>
        <div class="col-md-4">
            <select class="form-control" v-model="generalInfo.outOfStock">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
            </select>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Send me reports </label>
        <div class="col-md-4">
            <select class="form-control" v-model="generalInfo.wantsReportAt">
                <option value="1">Twice a day</option>
                <option value="2">Daily</option>
                <option value="3">Weekly</option>
                <option value="4">Twice a monthly</option>
                <option value="5">Monthly</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <input type="submit" class="col-md-4 btn btn-primary" value="Update" v-on:click.prevent="updateGeneralInfo" :disabled="loading">
        &nbsp;&nbsp;<img src="/assets/img/loader.gif" style="display: none;" v-show="loading">
    </div>

</form>