<form class="form-horizontal form-bordered" method="POST">
    <div class="form-group">
        <label class="col-md-3 control-label">Firm Name</label>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder="Firm Name" v-model="userDetails.provider_name" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Address</label>
        <div class="col-md-4">
            <textarea type="text" rows="3" class="form-control" placeholder="Your Contact Address" v-model="userDetails.firm_address" required></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Location</label>
        <div class="col-md-4">
            <div class="checkbox-custom checkbox-default">
                <input type="checkbox" id="checkboxExample1" v-model="userDetails.location">
                <label for="checkboxExample1">Check if this is where your store is located</label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <input type="submit" class="col-md-4 btn btn-primary" value="Update Info." v-on:click.prevent="updateInfo" :disabled="loading">
        &nbsp;&nbsp;<img src="/assets/img/loader.gif" style="display: none;" v-show="loading">
    </div>

</form>