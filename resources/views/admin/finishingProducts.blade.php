@extends('layouts.admin')

@section('content')
    <section role="main" class="content-body">
        <div class="row">
            <div class="col-sm-8">
                <a class="btn btn-default mb-xs mt-xs mr-xs" v-show="!selectedExists()" v-on:click="getAllOutOfStock"><i class="fa fa-refresh"></i></a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>

                        <h2 class="panel-title">All Out of stock products</h2>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb-none" v-show="outOfStockExists()">
                                <thead>
                                <th><input type="checkbox" v-model="selectAll" @click="checkReleased"></th>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Mfg Date</th>
                                <th>Expiry Date</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(product,index) in outOfStock" v-bind:class="{selected:isSelected(product.id)}"  class="products">
                                    <td><input type="checkbox" v-bind:value="product.id" v-bind:checked="selectAll" v-model="selected"></td>
                                    <td>@{{ index+1 }}</td>
                                    <td>@{{ product.product_name }}</td>
                                    <td></td>
                                    <td></td>
                                    <td class="actions-hover actions-fade">
                                        <a href="" title="Undo Release" v-if="isReleased(product.id)"><i class="fa fa-undo"></i></a>
                                        <a href="#!" class="delete-row" @click="findInterestedPharmacy()" title="find pharmacies that are interested"><i class="fa fa-search"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <p v-if="!getAllOutOfStock()">None of your products are out of stock yet</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@stop

@section('page_scripts')
    <script src="/admin-assets/javascripts/ui-elements/examples.modals.js"></script>
    <script src="/admin-assets/javascripts/forms/examples.advanced.form.js"></script>
@stop