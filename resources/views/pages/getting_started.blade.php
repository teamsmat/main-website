@extends('layouts.master')

@section('content')

    <section class="main-slider" data-stellar-background-ratio="0.5" style="background-image: url(images/headers/about.jpg)">
        <div class="slider-caption">
            <h2 data-animate="fadeInDown" data-delay="1000" data-duration="2s">DRUGS ARE NOW ONLINE, EVERYWHERE YOU ARE</h2>
            <a data-animate="fadeInUp" data-duration="2s" data-delay="1300" href="#packages" class="btn btn-primary btn-lg">CHECK OUT OUR PACKAGES</a>	</div>
    </section>    </header>
    <section class="hero-banner">
        <div class="container text-center">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h1 class="text-primary">YOU'RE DEFINITELY HEALTHIER NOW</h1>
                    <h2 class="text-default"></h2><hr>
                    <div class="row text-justify">
                        <div class="col-sm-6">
                            Umedics brings your drugs closer than ever before to you. Whenever you feel you've got a headache,
                            dont worry. Drugs are nearby. And we forgot to talk about price? it's absolutely free for users. Our
                            MObile application is coming soon and promises to give better experience.
                        </div>
                        <div class="col-sm-6">
                            <p>
                                Pharmacies can manage their stores on this platform. You know what this means? you can carry your store with you, anywhere
                                you go. Checking your products on the fly is not only beneficial to you...It helps us as well. We will never
                                buy expired drugs anymore.
                            </p>
                            <p>
                                And it doesn't have to be a pain to use these services afterall.Umedics offers you a premium 15 days trial after which you
                                can use the FREE or PREMIUM service. Click <a href="#packages" class="btn btn-primary">here</a> to view our pricing.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="video-block text-center">
        <div class="video-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h1 class="text-success">HOW TO USE...</h1>
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <p>By default,umedics filters results based on your location, hence, it is advisable to turn on your notification
                            setting. You can turn off this feature by unchecking the use location checkbox under the search bar. Its that easy. CLick the
                            button below to watch a demo video</p><br>
                            <p>
                                <a href="javascript:;" class="btn btn-default btn-bordered btn-pill">Watch Video</a>
                            </p>
                        </div>
                        <div class="col-sm-6 text-left">
                            <ul class="list-unstyled whyuslist">
                                <li><i class="icon-checkmark2 text-default"></i> Click the blue floating button</li>
                                <li><i class="icon-checkmark2 text-default"></i> Search any drug you want </li>
                                <li><i class="icon-checkmark2 text-default"></i> Suggestions will automatically be displayed</li>
                                <li><i class="icon-checkmark2 text-default"></i> OR click search or git enter</li>
                                <li><i class="icon-checkmark2 text-default"></i> Scroll through the results presented
                                    <a href="/tutorial">read more...</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <video loop="" muted="" autoplay="" data-src="images/video/Site_Map_Drawing_from_LIFEOFVIDS_on_296353493.mp4">
        </video>
    </section>

    @include('pages.pricing')


@stop