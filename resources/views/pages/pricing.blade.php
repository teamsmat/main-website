<section class="pricing-tables block" id="packages">
    <div class="container">
        <h2>OUR PACKAGES</h2><br>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="pricing text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-header">
                                <h3>TRIAL</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-price">
                                FREE<br>
                                <p class="text-sm">Enjoy all premium benefits for 15 days</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled pricing-features">
                            <li><i class="icon-star7"></i><i class="icon-star7"></i><i class="icon-star6"></i><i class="icon-star5"></i><i class="icon-star5"></i></li>
                            <li>Unlimited Products</li>
                            <li>Private Products</li>
                            <li>Inventory tracking</li>
                            <li>Email Notifications</li>
                            <li>Full Support</li>
                            <li>And more...</li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-buttons">
                                <a class="btn btn-block btn-primary">START</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="pricing text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-header">
                                <h3>BASIC</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-price">
                                FREE<br>
                                <p class="text-sm">Our free plan</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled pricing-features">
                            <li><i class="icon-star7"></i><i class="icon-star7"></i><i class="icon-star6"></i><i class="icon-star5"></i><i class="icon-star5"></i></li>
                            <li>Unlimited product uploads</li>
                            <li>Public Products</li>
                            <li>Single User</li>
                            <li>Profile, Product Statistics</li>
                            <li>Limited Support</li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-buttons">
                                <a class="btn btn-block btn-primary">START</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="pricing text-center pricing-selected">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-header">
                                <h3>PREMIUM</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-price">
                                <span class="pricing-currency">{{ currency() }}</span>{{ number_format(5000) }}<span class="pricing-period">/m</span><br>
                                <p class="text-sm">Enjoy everything umedics... Its worth your money.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled pricing-features">
                            <li><i class="icon-star7"></i><i class="icon-star7"></i><i class="icon-star7"></i><i class="icon-star7"></i><i class="icon-star7"></i></li>
                            <li>Unlimited Products</li>
                            <li>Private Products</li>
                            <li>Inventory tracking</li>
                            <li>Email Notifications</li>
                            <li>Full Support</li>
                            <li>Custom Profile</li>
                            <li>Emails to subscribers</li>
                            <li>and more</li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-buttons">
                                <a class="btn btn-block btn-primary">PURCHASE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="pricing text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-header">
                                <h3>EXTENDED</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-price">
                                    <span class="pricing-currency"></span><span class="pricing-period"></span><br>
                                <p class="text-sm">Not yet Available</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled pricing-features">
                            <li>We can customize this service for you when it goes live. Do you want to know when we launch this service?</li>

                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing-buttons">
                                <a class="btn btn-block btn-primary">NOTIFY ME</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>