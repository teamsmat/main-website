@extends('layouts.master')

@section('content')

	<section class="main-slider fullsize" data-stellar-background-ratio="0.5" style="background-image: url(/images/headers/index.jpg)">
	<div class="slider-caption">
		<h1 data-animate="fadeInDown" data-delay="1000" data-duration="2s">Your Health is very important <br>we consider that greatly!</h1>
        <a data-animate="fadeInUp" data-duration="2s" data-delay="1300" href="/get-started" class="btn btn-primary btn-lg">GET STARTED</a>
    </div>
</section>    </header>
    <section class="hero-banner">
        <div class="container text-center">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                <h2>Find any <span class="invert bg-success">DRUG</span>. We will serve them to you.</h2>
                <p>
                    With Umedics, you can find drugs from several pharmacies around you. We do not sell drugs. All we do is help you find the right drugs preferably at a location near you. Please contact us in case you have any complaints with any pharmacy.
                </p>
                </div>
            </div>
        </div>
    </section>


    <section class="services-block">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>WHAT WE OFFER</h2>
                </div>
            </div>
            <div class="row zoom-gallery" data-animate="zoomIn" data-delay="0">
                <div class="col-sm-4">
                    <a href="images/portfolio/image1.jpg" data-source="images/portfolio/image1.jpg" title="Some Project Information" class="gallery-item effect-milo">
                        <img src="images/portfolio/thumbs/image1.jpg">
                    </a>
                    <a href="javascript:;" class="gallery-item-title">Drug Finder</a>
                    <p class="gallery-item-descr">Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.</p>
                </div>
                <div class="col-sm-4" data-animate="zoomIn" data-delay="300">
                    <a href="images/portfolio/image2.jpg" data-source="images/portfolio/image2.jpg" title="Some Project Information" class="gallery-item effect-milo">
                        <img src="images/portfolio/thumbs/image2.jpg">
                    </a>
                    <a href="javascript:;" class="gallery-item-title">Hospital Finder</a>
                    <p class="gallery-item-descr">Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.</p>
                </div>
                <div class="col-sm-4" data-animate="zoomIn" data-delay="600">
                    <a href="images/portfolio/image3.jpg" title="Some Project Information" class="gallery-item effect-milo">
                        <img src="images/portfolio/thumbs/image3.jpg">
                    </a>
                    <a href="javascript:;" class="gallery-item-title">	Quick Doctor</a>
                    <p class="gallery-item-descr">Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials-block parallax-bg text-center" data-stellar-background-ratio="0.6">

        <div id="testimonial" class="owl-carousel owl-theme">
            <div class="item">
                <h2>"We really loved working with UMEDICS.<br>They know what they are doing and make<br> you feel that on every step."</h2>
                <p><b>Micheal Lichtenstein</b><br>CEO, WELLFUND CORPORATION</p>
            </div>
            <div class="item">
                <h2>"All their work are awesome and they are <br>really sensitive about customer happiness.<br> Never seen a support like this before."</h2>
                <p><b>William Winfrey</b><br>Marketing Manager, DONTDROPBOX</p>
            </div>
            <div class="item">
                <h2>"We really loved working with UMEDICS.<br>They know what they are doing and make<br> you feel that on every step."</h2>
                <p><b>Micheal Lichtenstein</b><br>CEO, WELLFUND CORPORATION</p>
            </div>
            <div class="item">
                <h2>"All their work are awesome and they are <br>really sensitive about customer happiness.<br> Never seen a support like this before."</h2>
                <p><b>William Winfrey</b><br>Marketing Manager, DONTDROPBOX</p>
            </div>

        </div>
    </section>

    <section class="brands-block text-center">
        <div class="container">
            <h1 class="text-success">Pharmacies currently using UMEDICS</h1>
            <div id="brands" class="owl-carousel owl-theme">
                <div class="item">
                    <a href="javascript:;" title="Coca Cola">
                        <img class="image-sized" src="images/brands/cocacola.png">
                    </a>
                </div>

                <div class="item">
                    <a href="javascript:;" title="Microsoft">
                        <img class="image-sized" src="images/brands/microsoft.png">
                    </a>
                </div>

                <div class="item">
                    <a href="javascript:;" title="Samsung">
                        <img class="image-sized" src="images/brands/samsung.png">
                    </a>
                </div>

                <div class="item">
                    <a href="javascript:;" title="Google">
                        <img class="image-sized" src="images/brands/google.png">
                    </a>
                </div>

                <div class="item">
                    <a href="javascript:;" title="Sony">
                        <img class="image-sized" src="images/brands/sony.png">
                    </a>
                </div>

                <div class="item">
                    <a href="javascript:;" title="Virgin">
                        <img class="image-sized" src="/images/brands/virgin.png">
                    </a>
                </div>

                <div class="item">
                    <a href="javascript:;" title="Instragram">
                        <img class="image-sized" src="/images/brands/instagram.png">
                    </a>
                </div>
            </div>
        </div>
    </section>

        <section class="subscribe-block text-center">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-6 col-sm-offset-3">
	                    <h1>SUBSCRIBE</h1>
	                    <p>Please subscribe to our newsletter so you can learn the updates for new products before anyone else.</p>
	                    <hr class="divider">
	                    <form action="/newsletter" role="form" method="POST">

	                    	{{ csrf_field() }}
                            <input type="email" placeholder="Enter your email address" name="email" id="email" class="form-control input-lg" required>
                            <button class="btn btn-primary btn-lg subscribe-btn" type="submit">Submit</button>
                            <!--  onsubmit="Subscribe()" -->
	                    </form>
	                </div>
                 </div>
	        </div>
	    </section>

    @include('pages.homepage_flash')

@stop

@section('scripts')

    <script>

        $(document).ready(function(){
            setTimeout(function(){
                $('#homepage_flash').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },6000);
        });

    </script>

@stop