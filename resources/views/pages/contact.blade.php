@extends('layouts.master')

@section('content')

        <section class="main-slider" data-stellar-background-ratio="0.5" style="background-image: url(images/headers/contact.jpg)">
            <div class="slider-caption">
                <h2 data-animate="fadeInDown" data-delay="1000" data-duration="2s">We are happy to answer<br>your questions and to be in <br><span class="invert bg-danger">contact</span> with you!</h2>
                <a data-animate="fadeInUp" data-duration="2s" data-delay="1300" href="#contact-form" class="btn btn-primary btn-lg">CONTACT US</a>	</div>
        </section>
    </header>


    <section id="contact-form" class="contact-form block">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <form name="" id="contactForm" novalidate>
                        <legend>GET IN TOUCH</legend>
                        <div id="success"></div> <!-- For success/fail messages -->
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" class="form-control" placeholder="Name" id="name" name="name">
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <input type="email" class="form-control" placeholder="Email" id="email" name="email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <textarea rows="10" cols="100" class="form-control" id="message" name="message" style="resize:none"></textarea>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="reset" class="btn btn-danger">RESET</button>
                            <button type="button" class="btn btn-primary">SEND</button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="well transparent">
                                <h4>OFFICE ADDRESS</h4>

                                <b>ADDRESS</b>
                                <p>14 Lalubu Road, Oke-ilewo Abeokuta</p>
                                <b>PHONE</b>
                                <p>07058286311</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop