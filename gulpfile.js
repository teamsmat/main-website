var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');

    mix.styles([
    	'bootstrap/css/bootstrap.min.css',
    	'pace/pace.css',
    	'animate-css/animate.min.css',
    	'iconmoon/style.css',
    	'owl-carousel/owl.carousel.css',
    	'owl-carousel/owl.theme.css',
    	'owl-carousel/owl.transitions.css',
    	'jquery-magnific/magnific-popup.css',
    	'style.css',
        'specials.css'
    ]);

    mix.scripts([
        'less-js/less-1.7.5.min.js',
        'pace/pace.min.js',
        'jquery/jquery-1.11.1.min.js',
        'bootstrap/js/bootstrap.min.js',
        'jquery-browser/jquery.browser.min.js',
        'fastclick/fastclick.js',
        'stellarjs/jquery.stellar.min.js',
        'jquery-appear/jquery.appear.js',
        'js/init.js',
        'owl-carousel/owl.carousel.min.js',
        'jquery-magnific/jquery.magnific-popup.min.js',
        'js/pages/index.js'
    ]);
});
