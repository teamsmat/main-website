/**
 * Created by tosin on 10/1/16.
 */

var prod = new Vue({
    el:'.body',
    http:{
      root:'/',
        headers:{
            'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
        }
    },
    methods:{
        notify: function(message) {
            alert(message);
        },

        refresh: function() {
            this.fetchAllData();
        },
        finishSetup: function () {
            if(this.termsOfService){
                this.persistSetupData();
                this.redirectTo('products');
            }
        },

        persistSetupData: function() {
            this.$http.post('/1/setup',this.userDetails,function(response) {
                alert(response);
            });
        },

        productExists: function() {
            return this.exists(this.products);
        },

        expirationExists: function() {
            return this.exists(this.expiration);
        },

        outOfStockExists: function() {
            return this.exists(this.outOfStock);
        },

        exists: function(data) {
            return data.length > 0;
        },

        getAllProducts: function()  {
            var self = this;
            $.get('/smat_pharm/products',function(data) {
              self.products = data;
            })
        },

        getAllExpiration: function() {
            var self = this;
            $.get('/smat_pharm/expiration',function(data) {
                self.expiration = data;
            })
        },

        getAllReleased: function() {
            var self = this;
            $.get('/smat_pharm/expiration/released',function(data) {
                self.released = data;
            })
        },

        getAllOutOfStock:function() {
            var self = this;
            $.get('/smat_pharm/products/finishing',function(data) {
                self.outOfStock = data;
            });
        },

        isReleased: function(product) {
            return this.isInArray(product,this.released);
        },

        saveNewProduct: function() {
            var btn = $('#btnSaveProduct');
            this.disable(btn);
            this.$http.post('/smat_pharm/products/newProduct/',this.newSingleProduct);
            this.fetchAllData();
            this.newSingleProduct = {};
            this.enable(btn);
        },

        deleteSelected: function() {
            var conf = confirm(this.selected.length+ ' Products will be discarded. Continue anyway?');
            if(conf){
                this.deleteProduct();
            }
        },

        deleteSingle:function(id) {
            var conf = confirm("Are you sure you want to remove this product? "+id);
            if(conf)
            {
                this.selected.push(id);
                this.deleteProduct();
            }
        },

        deleteProduct: function() {
            var dataToDelete = this.selectedJSON();
            this.$http.delete('/smat_pharm/products/'+dataToDelete);
            this.fetchAllData();
            this.clearSelected();
        },

        selectedJSON: function() {
          return JSON.stringify(this.selected);
        },

        clearSelected: function() {
            this.selected = [];
        },

        checkReleased: function() {
            this.check(this.expiration);
        },

        check: function(array) {
            if(!this.selectAll){
                for(product in array){
                    this.selected.push(array[product].id);
                }
            }else{
                this.selected = [];
            }
            this.selectAll = !this.selectAll;
        },

        checkAll:function() {
            this.check(this.products);
        },

        isSelected: function(id) {
            if(this.isInArray(id,this.selected)){
                return true;
            }
        },

        selectedExists: function() {
            return this.selected.length > 0;
        },

        isInArray: function(value,array) {
            return array.indexOf(value) > -1;
        },

        disable: function(btn) {
            btn.attr('disabled','disabled');
            btn.html('saving');
        },

        enable: function(btn) {
            btn.removeAttr('disabled');
            btn.html('Save Product');
        },

        redirectTo: function(route) {
            window.location.href = "/"+this.userDetails.username+"/"+route;
        },

        releaseProducts: function() {
            var dataToRelease = this.selectedJSON();
            this.$http.post('/therealsmat/products/release',this.selected);
            this.getAllReleased();
            this.selected = [];
        },

        fetchAllData: function() {
            this.getAllProducts();
            this.getAllExpiration();
            this.getAllReleased();
            this.getAllOutOfStock();
        },

        updateInfo: function() {
            this.persistSetupData();
        },

        updateGeneralInfo: function() {
            alert("I have been clicked now. So what?");
            console.log(this.generalInfo);
        },

        load: function() {
            this.loading = true;
        },

        stopLoading: function() {
            this.loading = false;
        }
    },
    data:function(){
        return {
            userDetails:{},
            generalInfo:{},
            newSingleProduct:{},
            loading:false,
            termsOfService:false,
            products:{},
            expiration:{},
            outOfStock:{},
            selectAll:false,
            selected:[],
            released:[]
        }
    },
    created: function() {
        this.fetchAllData();
    }
});

Vue.http.interceptors.push(function (request, next) {
    this.load();
    next(function (response){
        this.stopLoading();
    });
});
