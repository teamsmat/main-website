var connect = new Vue({
    el:'.body',
    http:{
        root:'/',
        headers:{
            'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
        }
    },
    methods:{
        getConnections:function() {
            var self = this;
            $.get('/smat_pharm/connect',function(data) {
                self.connections = data;
            })
        }
    },
    data:function() {
        connections:{}
    },
    created:function() {
        this.getConnections();
    }
});
