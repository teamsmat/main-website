<?php

namespace Database\seeds;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    public function run()
    {
        factory('App\Products',50)->create();
    }
}