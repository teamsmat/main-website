<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_owner');
            $table->string('product_name');
            $table->integer('therapeutic_class');
            $table->integer('qty');
            $table->float('cost_price');
            $table->float('selling_price');
            $table->date('mfg_date');
            $table->date('exp_date');
            $table->integer('visibility');
            $table->string('batch_no');
            $table->boolean('active');
            $table->boolean('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
