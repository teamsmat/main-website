<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsletterSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailSubscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->text('subscriber_details'); /* This holds the browsin details of the subscriber*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emailSubscribers');
    }
}
